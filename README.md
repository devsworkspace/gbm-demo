# GBM Demo

Application made by Alejandro Salazar for the GBM CodeChallenge

## Requisites

* Android Studio Bumblebee | 2021.1.1 Beta 1
* Java 1.8
* Gradle 7.2
* Min SDK version support: **26**
* Target SDK version: **31**

## Features

* Let’s Rock (Login by PIN or fingerprint)
* Dark theme mode support.
* English(USA) and Spanish language support.
* Cache support.
* Piece of Cake (Show consumer price index chart).
* Logout confirmation.
* Toolbar menu controller.
* Bottom navigation + Navigation Controller(Jetpack).
* Retry connection.
* Damm I’m Good (Show top ten financial products and auto reload).
* Automatic testing.
* Crashlytic.
* Ofuscation code.

## Dependencies

* [Android Jetpack](https://developer.android.com/jetpack)
* [Timber](https://github.com/JakeWharton/timber)
* [LeakCanary](https://square.github.io/leakcanary/)
* [Retrofit](https://square.github.io/retrofit/)
* [Moshi](https://github.com/square/moshi)
* [Logging Interceptor](https://github.com/square/okhttp/tree/master/okhttp-logging-interceptor)
* [MPAndroidChart](https://github.com/PhilJay/MPAndroidChart)
* [KMNumbers](https://github.com/ShootrNetwork/kmnumbers)
* [Lottie](https://lottiefiles.com/blog/working-with-lottie/getting-started-with-lottie-animations-in-android-app)
* [ViewPager2](https://developer.android.com/jetpack/androidx/releases/viewpager2)

## Version

The application's version is **1.0.0** (*versionCode*: 1)

The version format is [Semantic Versioning 2.0.0](https://semver.org/)


