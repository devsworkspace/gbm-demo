package com.alex.gbmdemo

import com.alex.data.entities.ConsumerPriceIndexEntity
import com.alex.data.response.ConsumerPriceIndexResponse
import java.util.*

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 24/11/21.
 */
object DataDummy {

    fun getConsumerPriceEntity(): List<ConsumerPriceIndexEntity> {
        val examples = mutableListOf<ConsumerPriceIndexEntity>()




        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39285.85,
            percentageChange = 0.86257,
            volume = 128684937,
            change = 335.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39337.65,
            percentageChange = 0.13,
            volume = 78905,
            change = 51.8
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39300.94,
            percentageChange = 0.04,
            volume = 129403,
            change = 15.09
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39313.04,
            percentageChange = 0.07,
            volume = 145518,
            change = 27.19
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39280.85,
            percentageChange = -0.01,
            volume = 200604,
            change = -5.0
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39277.69,
            percentageChange = -0.02,
            volume = 239454,
            change = -8.16
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39261.24,
            percentageChange = -0.06,
            volume = 275411,
            change = -24.61
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39251.35,
            percentageChange = -0.09,
            volume = 301208,
            change = -34.5
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39245.85,
            percentageChange = -0.1,
            volume = 333577,
            change = -40.0
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39251.66,
            percentageChange = -0.09,
            volume = 409938,
            change = -34.19
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39242.11,
            percentageChange = -0.11,
            volume = 445813,
            change = -43.74
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39244.5,
            percentageChange = -0.11,
            volume = 521588,
            change = -41.35
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39230.65,
            percentageChange = -0.14,
            volume = 748834,
            change = -55.2
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39229.91,
            percentageChange = -0.14,
            volume = 970438,
            change = -55.94
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39234.4,
            percentageChange = -0.13,
            volume = 1114821,
            change = -51.45
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39230.35,
            percentageChange = -0.14,
            volume = 1280705,
            change = -55.5
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39241.91,
            percentageChange = -0.11,
            volume = 1312243,
            change = -43.94
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39233.98,
            percentageChange = -0.13,
            volume = 1450191,
            change = -51.87
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39242.52,
            percentageChange = -0.11,
            volume = 1901141,
            change = -43.33
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39235.47,
            percentageChange = -0.13,
            volume = 2073078,
            change = -50.38
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39219.02,
            percentageChange = -0.17,
            volume = 2209707,
            change = -66.83
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39226.5,
            percentageChange = -0.15,
            volume = 2326819,
            change = -59.35
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39216.07,
            percentageChange = -0.18,
            volume = 2513260,
            change = -69.78
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39214.77,
            percentageChange = -0.18,
            volume = 2639966,
            change = -71.08
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39228.82,
            percentageChange = -0.15,
            volume = 2745713,
            change = -57.03
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39223.43,
            percentageChange = -0.16,
            volume = 2795632,
            change = -62.42
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39209.31,
            percentageChange = -0.19,
            volume = 2970378,
            change = -76.54
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39202.66,
            percentageChange = -0.21,
            volume = 3098829,
            change = -83.19
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39202.68,
            percentageChange = -0.21,
            volume = 3157756,
            change = -83.17
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39197.04,
            percentageChange = -0.23,
            volume = 3544626,
            change = -88.81
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39200.34,
            percentageChange = -0.22,
            volume = 3569171,
            change = -85.51
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39189.08,
            percentageChange = -0.25,
            volume = 3642988,
            change = -96.77
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39183.09,
            percentageChange = -0.26,
            volume = 3725528,
            change = -102.76
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39197.74,
            percentageChange = -0.22,
            volume = 4339011,
            change = -88.11
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39178.32,
            percentageChange = -0.27,
            volume = 5108851,
            change = -107.53
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39184.05,
            percentageChange = -0.26,
            volume = 5259929,
            change = -101.8
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39197.64,
            percentageChange = -0.22,
            volume = 5392160,
            change = -88.21
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39196.02,
            percentageChange = -0.23,
            volume = 5473335,
            change = -89.83
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39193.71,
            percentageChange = -0.23,
            volume = 5491839,
            change = -92.14
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39193.99,
            percentageChange = -0.23,
            volume = 5557152,
            change = -91.86
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39184.07,
            percentageChange = -0.26,
            volume = 5601277,
            change = -101.78
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39183.0,
            percentageChange = -0.26,
            volume = 5627224,
            change = -102.85
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39180.53,
            percentageChange = -0.27,
            volume = 5703439,
            change = -105.32
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39189.52,
            percentageChange = -0.25,
            volume = 5946536,
            change = -96.33
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39171.53,
            percentageChange = -0.29,
            volume = 6034831,
            change = -114.32
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39168.72,
            percentageChange = -0.3,
            volume = 6087426,
            change = -117.13
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39155.48,
            percentageChange = -0.33,
            volume = 6134779,
            change = -130.37
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39154.55,
            percentageChange = -0.33,
            volume = 6255868,
            change = -131.3
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39136.73,
            percentageChange = -0.38,
            volume = 6447822,
            change = -149.12
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39134.83,
            percentageChange = -0.38,
            volume = 6759867,
            change = -151.02
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39127.09,
            percentageChange = -0.4,
            volume = 6857638,
            change = -158.76
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39126.51,
            percentageChange = -0.41,
            volume = 6906199,
            change = -159.34
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39132.71,
            percentageChange = -0.39,
            volume = 7054210,
            change = -153.14
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39135.44,
            percentageChange = -0.38,
            volume = 7373600,
            change = -150.41
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39136.76,
            percentageChange = -0.38,
            volume = 7693139,
            change = -149.09
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39154.89,
            percentageChange = -0.33,
            volume = 7773159,
            change = -130.96
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39156.17,
            percentageChange = -0.33,
            volume = 7814075,
            change = -129.68
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39153.97,
            percentageChange = -0.34,
            volume = 7864949,
            change = -131.88
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39182.0,
            percentageChange = -0.26,
            volume = 8032029,
            change = -103.85
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39173.74,
            percentageChange = -0.29,
            volume = 8088822,
            change = -112.11
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39161.18,
            percentageChange = -0.32,
            volume = 8239104,
            change = -124.67
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39149.16,
            percentageChange = -0.35,
            volume = 8780577,
            change = -136.69
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39143.16,
            percentageChange = -0.36,
            volume = 9235048,
            change = -142.69
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39141.14,
            percentageChange = -0.37,
            volume = 9286640,
            change = -144.71
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39121.46,
            percentageChange = -0.42,
            volume = 9318440,
            change = -164.39
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39123.92,
            percentageChange = -0.41,
            volume = 9386726,
            change = -161.93
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39129.22,
            percentageChange = -0.4,
            volume = 9474403,
            change = -156.63
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39133.47,
            percentageChange = -0.39,
            volume = 9662701,
            change = -152.38
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39123.34,
            percentageChange = -0.41,
            volume = 9688164,
            change = -162.51
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39121.1,
            percentageChange = -0.42,
            volume = 10845458,
            change = -164.75
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39128.12,
            percentageChange = -0.4,
            volume = 10935279,
            change = -157.73
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39122.24,
            percentageChange = -0.42,
            volume = 11089014,
            change = -163.61
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39138.85,
            percentageChange = -0.37,
            volume = 11334506,
            change = -147.0
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39142.05,
            percentageChange = -0.37,
            volume = 11392371,
            change = -143.8
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39146.84,
            percentageChange = -0.35,
            volume = 11464372,
            change = -139.01
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39140.7,
            percentageChange = -0.37,
            volume = 11860627,
            change = -145.15
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39142.98,
            percentageChange = -0.36,
            volume = 12021898,
            change = -142.87
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39139.27,
            percentageChange = -0.37,
            volume = 12144322,
            change = -146.58
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39130.83,
            percentageChange = -0.39,
            volume = 12705502,
            change = -155.02
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39108.88,
            percentageChange = -0.45,
            volume = 13030223,
            change = -176.97
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39123.1,
            percentageChange = -0.41,
            volume = 13219802,
            change = -162.75
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39117.84,
            percentageChange = -0.43,
            volume = 13305336,
            change = -168.01
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39113.98,
            percentageChange = -0.44,
            volume = 13355546,
            change = -171.87
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39101.7,
            percentageChange = -0.47,
            volume = 13606767,
            change = -184.15
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39106.77,
            percentageChange = -0.46,
            volume = 13725315,
            change = -179.08
        ))
        examples.add(ConsumerPriceIndexEntity(
            date = Date().time,
            price = 39100.58,
            percentageChange = -0.47,
            volume = 13786034,
            change = -185.27
        ))


        examples.shuffle()
        return examples
    }
}