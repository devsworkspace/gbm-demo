package com.alex.gbmdemo.data.endpoints

import com.alex.data.endpoints.CodeChallengeEndpoint
import com.alex.gbmdemo.providers.*
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 24/11/21.
 */

@HiltAndroidTest
@UninstallModules(
    RepositoryProvider::class,
    ApiProvider::class,
    EndpointProvider::class,
    DaoProvider::class,
    RetrofitProvider::class,
    UseCaseProvider::class
)
class CodeChallengeEndpointTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var codeChallengeEndpoint: CodeChallengeEndpoint

    @Before
    fun setup() {
        hiltRule.inject()
    }

    @Test
    fun getConsumerPriceEndpoint() = runBlocking {
        val consumerPriceHistory = codeChallengeEndpoint.getConsumerPriceHistory()
        assertThat(consumerPriceHistory).isNotEmpty()
    }

    @Test
    fun getFinancialProductEndpoint() = runBlocking {
        assertThat(codeChallengeEndpoint.getFinancialProducts()).isNotEmpty()
    }

}