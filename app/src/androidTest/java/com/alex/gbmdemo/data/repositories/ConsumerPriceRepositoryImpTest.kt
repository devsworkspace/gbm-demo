package com.alex.gbmdemo.data.repositories

import com.alex.data.apis.CodeChallengeApi
import com.alex.data.daos.ConsumerPriceIndexDao
import com.alex.data.endpoints.CodeChallengeEndpoint
import com.alex.data.repositories.ConsumerPriceRepositoryImp
import com.alex.domain.repositories.ConsumerPriceRepository
import com.alex.gbmdemo.DataDummy
import com.alex.gbmdemo.providers.*
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException
import javax.inject.Inject

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 24/11/21.
 */
@HiltAndroidTest
@UninstallModules(
    RepositoryProvider::class,
    ApiProvider::class,
    EndpointProvider::class,
    DaoProvider::class,
    RetrofitProvider::class,
    UseCaseProvider::class
)
@RunWith(MockitoJUnitRunner::class)
class ConsumerPriceRepositoryImpTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var consumerPriceRepository: ConsumerPriceRepository

    @Inject
    lateinit var consumerPriceIndexDao: ConsumerPriceIndexDao

    @Mock
    lateinit var codeChallengeEndpoint: CodeChallengeEndpoint

    @Before
    fun setup() {
        hiltRule.inject()
    }

    @Test
    fun getConsumerPriceIndex() = runBlocking {
        assertThat(consumerPriceRepository.getConsumerPriceHistory()).isNotNull()
    }

    @Test
    fun getLocalInfo(): Unit = runBlocking {
        val data = DataDummy.getConsumerPriceEntity()
        consumerPriceIndexDao.updateLocalInfo(data)
        try {
            Mockito.`when`(codeChallengeEndpoint.getConsumerPriceHistory())
                .thenThrow(IOException::class.java)
        } catch (throwable: Throwable) {
        }

        val valueData = ConsumerPriceRepositoryImp(
            CodeChallengeApi(codeChallengeEndpoint),
            consumerPriceIndexDao
        ).getConsumerPriceHistory()

        assertThat(valueData.localInfo).isTrue()
        assertThat(valueData.consumerPriceIndex.size).isEqualTo(data.size)

    }


}