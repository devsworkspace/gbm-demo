package com.alex.gbmdemo.data.repositories

import com.alex.domain.repositories.FinancialProductRepository
import com.alex.gbmdemo.providers.*
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertThrows
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 23/11/21.
 */
@HiltAndroidTest
@UninstallModules(
    RepositoryProvider::class,
    ApiProvider::class,
    EndpointProvider::class,
    DaoProvider::class,
    RetrofitProvider::class,
    UseCaseProvider::class
)
class FinancialProductRepositoryImpTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var financialProductRepository: FinancialProductRepository

    @Before
    fun setup() {
        hiltRule.inject()
    }

    @Test
    fun getFinancialProductListToApi() = runBlocking {
        val firstResult = financialProductRepository.getFlowTopFinancialProduct(5000).first()
        assertTrue(firstResult.isNotEmpty())
    }

    @Test(timeout = 55000)
    fun validateRepeatGetFinancialProductList() {
        var repeats = 0
        val minRepeatsRequire = 10
        assertThrows(CancellationException::class.java) {
            runBlocking {
                financialProductRepository
                    .getFlowTopFinancialProduct(5000).collect {
                        if (!it.isNullOrEmpty())
                            repeats++
                        if (repeats == minRepeatsRequire)
                            cancel()
                    }
            }
        }
    }


}