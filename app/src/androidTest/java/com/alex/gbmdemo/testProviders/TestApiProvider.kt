package com.alex.gbmdemo.testProviders

import com.alex.data.apis.CodeChallengeApi
import com.alex.data.endpoints.CodeChallengeEndpoint
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@Module
@InstallIn(SingletonComponent::class)
object TestApiProvider {

    @Singleton
    @Provides
    fun provideCodeChallengeApiProvider(
        codeChallengeEndpoint: CodeChallengeEndpoint
    ): CodeChallengeApi {
        return CodeChallengeApi(codeChallengeEndpoint)
    }

}