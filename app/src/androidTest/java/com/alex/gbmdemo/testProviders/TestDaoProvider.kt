package com.alex.gbmdemo.testProviders

import android.content.Context
import androidx.room.Room
import com.alex.data.daos.ConsumerPriceIndexDao
import com.alex.data.databases.ConsumerPriceDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@Module
@InstallIn(SingletonComponent::class)
object TestDaoProvider {

    @Provides
    @Singleton
    fun provideConsumerPriceDatabase(
        @ApplicationContext context: Context
    ): ConsumerPriceDatabase {
        return Room.databaseBuilder(
            context,
            ConsumerPriceDatabase::class.java,
            ConsumerPriceDatabase.DATABASE_NAME
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideConsumerPriceDao(
        consumerPriceDatabase: ConsumerPriceDatabase
    ): ConsumerPriceIndexDao {
        return consumerPriceDatabase.getConsumerPriceIndexDao()
    }
}