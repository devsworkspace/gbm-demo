package com.alex.gbmdemo.testProviders

import com.alex.data.endpoints.CodeChallengeEndpoint
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 20/11/21.
 */
@Module
@InstallIn(SingletonComponent::class)
object TestEndpointProvider {

    @Singleton
    @Provides
    fun provideCodeChallengeEndpoint(
        retrofit: Retrofit
    ): CodeChallengeEndpoint {
        return retrofit.create(CodeChallengeEndpoint::class.java)
    }

}