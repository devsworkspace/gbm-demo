package com.alex.gbmdemo.providers

import com.alex.domain.repositories.ConsumerPriceRepository
import com.alex.domain.repositories.FinancialProductRepository
import com.alex.domain.usecases.GetConsumerPriceHistoryUseCase
import com.alex.domain.usecases.GetFinancialProductsTop
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@Module
@InstallIn(SingletonComponent::class)
object TestUseCaseProvider {

    @Singleton
    @Provides
    fun provideGetConsumerPriceHistoryUseCase(
        consumerPriceRepository: ConsumerPriceRepository
    ): GetConsumerPriceHistoryUseCase {
        return GetConsumerPriceHistoryUseCase(consumerPriceRepository)
    }

    @Singleton
    @Provides
    fun provideGetFinancialProductsTop(
        financialProductRepository: FinancialProductRepository
    ): GetFinancialProductsTop {
        return GetFinancialProductsTop(financialProductRepository)
    }

}