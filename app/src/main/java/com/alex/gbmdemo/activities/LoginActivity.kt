package com.alex.gbmdemo.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.alex.gbmdemo.BuildConfig
import com.alex.gbmdemo.R
import com.google.firebase.crashlytics.FirebaseCrashlytics

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        FirebaseCrashlytics
            .getInstance()
            .setCrashlyticsCollectionEnabled(!BuildConfig.DEBUG)
    }
}