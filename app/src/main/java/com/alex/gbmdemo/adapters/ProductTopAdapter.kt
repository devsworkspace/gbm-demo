package com.alex.gbmdemo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alex.domain.models.FinancialProduct
import com.alex.gbmdemo.databinding.ItemFinancialProductBinding

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 24/11/21.
 */
class ProductTopAdapter : RecyclerView.Adapter<ProductTopAdapter.ProductTopViewHolder>() {

    var products: MutableList<FinancialProduct>? = null
        set(value) {
            if (field == null || value == null) {
                field = value
                notifyDataSetChanged()
                return
            }

            if (field!! == value)
                return

            for (i in 0 until field!!.size) {
                if (field!![i] == value[i])
                    continue
                field!![i] = value[i]
                notifyItemChanged(i)
            }
        }

    data class ProductTopViewHolder(val binding: ItemFinancialProductBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ProductTopViewHolder(
        ItemFinancialProductBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: ProductTopViewHolder, position: Int) {
        holder.binding.product = products?.get(position)
    }

    override fun getItemCount() = products?.size ?: 0

}