package com.alex.gbmdemo.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.alex.gbmdemo.fragments.TopIncreasesFragment
import com.alex.gbmdemo.fragments.TopLowsFragment
import com.alex.gbmdemo.fragments.TopVolumeFragment

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 24/11/21.
 */
class TopFragmentAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount() = 3

    override fun createFragment(position: Int) = when (position) {
        0 -> TopIncreasesFragment()
        1 -> TopLowsFragment()
        else -> TopVolumeFragment()
    }

}