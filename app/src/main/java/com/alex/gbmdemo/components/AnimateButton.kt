package com.alex.gbmdemo.components

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.animation.AnimationUtils
import androidx.appcompat.widget.AppCompatButton
import com.alex.gbmdemo.R

class AnimateButton(context: Context, attrs: AttributeSet) :
    AppCompatButton(context, attrs) {


    private val scaleAnimationOnToucheActionDown =
        AnimationUtils.loadAnimation(context, R.anim.scale_down_button)

    private val scaleAnimationOnToucheActionUp =
        AnimationUtils.loadAnimation(context, R.anim.scale_up_button)

    init {
        scaleAnimationOnToucheActionDown.fillAfter = true
        scaleAnimationOnToucheActionDown.isFillEnabled = true
        scaleAnimationOnToucheActionUp.fillAfter = true
        scaleAnimationOnToucheActionUp.isFillEnabled = true

        if (!this.isEnabled)
            this.alpha = 0.3f
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null && this.isEnabled)
            when (event.action) {
                MotionEvent.ACTION_DOWN -> this.startAnimation(scaleAnimationOnToucheActionDown)
                MotionEvent.ACTION_CANCEL,
                MotionEvent.ACTION_UP -> this.startAnimation(
                    scaleAnimationOnToucheActionUp
                )
            }

        return super.onTouchEvent(event)
    }


}