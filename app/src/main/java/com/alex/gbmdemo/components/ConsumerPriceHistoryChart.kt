package com.alex.gbmdemo.components

import android.content.Context
import android.util.AttributeSet
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import com.alex.domain.models.ConsumerPriceIndex
import com.alex.domain.models.ConsumerPriceIndexHistory
import com.alex.gbmdemo.R
import com.alex.gbmdemo.extensions.localeTimeFormat
import com.alex.gbmdemo.extensions.toShortSocialNetwork
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import timber.log.Timber
import java.time.format.FormatStyle
import java.util.*


/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 22/11/21.
 */
@BindingMethods(
    value = [
        BindingMethod(
            type = ConsumerPriceHistoryChart::class,
            attribute = "chart:history",
            method = "setConsumerPriceIndexHistory"
        ),
        BindingMethod(
            type = ConsumerPriceHistoryChart::class,
            attribute = "chart:onSelected",
            method = "setOnConsumerSelected"
        )
    ]
)
class ConsumerPriceHistoryChart(context: Context, attrs: AttributeSet) : LineChart(context, attrs) {

    private var entries: List<ConsumerPriceEntry>? = null
    var onConsumerSelected: ((ConsumerPriceIndex) -> Unit)? = null
    var consumerPriceIndexHistory: ConsumerPriceIndexHistory? = null
        set(value) {
            field = value
            entries = if (field != null) {
                field?.consumerPriceIndex?.map { ConsumerPriceEntry(it) }
            } else null
            data = LineData(
                arrayListOf<ILineDataSet>(LineDataSet(entries, "")
                    .apply {
                        setDrawCircles(false)
                        setDrawValues(false)
                        lineWidth = 1.3f
                        color = context.getColor(R.color.general_text_color)
                    })
            )
            invalidate()
            notifyDataSetChanged()
        }

    init {
        description.isEnabled = false
        setScaleEnabled(false)
        legend.isEnabled = false

        axisRight.apply {
            setDrawZeroLine(true)
            setDrawLabels(false)
        }
        axisLeft.apply {
            labelCount = 5
            textColor = context.getColor(R.color.general_text_color)
            valueFormatter = object : ValueFormatter() {
                override fun getAxisLabel(value: Float, axis: AxisBase?): String {
                    return value.toShortSocialNetwork()
                }
            }
        }
        xAxis.apply {
            labelCount = 3
            textColor = context.getColor(R.color.general_text_color)
            position = XAxis.XAxisPosition.BOTTOM
            valueFormatter = object : ValueFormatter() {
                override fun getAxisLabel(value: Float, axis: AxisBase?): String {
                    return Date(value.toLong()).localeTimeFormat(FormatStyle.MEDIUM)
                }
            }
        }

        setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                if (e is ConsumerPriceEntry)
                    onConsumerSelected?.invoke(e.consumerPriceIndex)
            }

            override fun onNothingSelected() {
                Timber.d("onNothingSelected")
            }

        })

    }

    private data class ConsumerPriceEntry(
        val consumerPriceIndex: ConsumerPriceIndex
    ) : Entry(consumerPriceIndex.date.time.toFloat(), consumerPriceIndex.price.toFloat())


}