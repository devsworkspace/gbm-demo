package com.alex.gbmdemo.components

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import com.alex.gbmdemo.extensions.currencyFormat

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@BindingMethods(
    value = [
        BindingMethod(
            type = CurrencyTextView::class,
            attribute = "value",
            method = "setValue"
        )
    ]
)
class CurrencyTextView(context: Context, attributeSet: AttributeSet) :
    AppCompatTextView(context, attributeSet) {

    var value: Double? = null
        set(value) {
            field = value

            text = if (field != null && field != 0.0)
                field?.currencyFormat()
            else
                ""
        }
}