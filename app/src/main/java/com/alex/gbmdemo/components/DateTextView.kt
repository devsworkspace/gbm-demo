package com.alex.gbmdemo.components

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import com.alex.gbmdemo.R
import com.alex.gbmdemo.extensions.localeDateFormat
import com.alex.gbmdemo.extensions.localeTimeFormat
import java.time.format.FormatStyle
import java.util.*

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@BindingMethods(
    value = [
        BindingMethod(
            type = DateTextView::class,
            attribute = "app:date",
            method = "setValue"
        )
    ]
)
class DateTextView(context: Context, attributeSet: AttributeSet) :
    AppCompatTextView(context, attributeSet) {

    companion object {
        private const val DATE = 1
        private const val TIME = 0
    }

    private var typeInfo = DATE

    init {
        val typedArray =
            context.theme.obtainStyledAttributes(attributeSet, R.styleable.DateTextView, 0, 0)
        typeInfo = typedArray.getInt(R.styleable.DateTextView_type, 1)
        typedArray.recycle()
    }

    var value: Date? = null
        set(value) {
            field = value

            text = if (field != null)
                (if (typeInfo == DATE) field?.localeDateFormat(FormatStyle.FULL)
                else field?.localeTimeFormat(FormatStyle.FULL))
                    ?.replaceFirstChar { if (it.isLowerCase()) it.uppercase() else it.toString() }
            else
                ""
        }
}