package com.alex.gbmdemo.components

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.alex.gbmdemo.R

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 22/11/21.
 */
class LoaderView(context: Context, attrs: AttributeSet? = null) : ConstraintLayout(context, attrs) {
    init {
        inflate(context, R.layout.layout_loader, this)
        findViewById<View>(R.id.lout_loader).setOnClickListener {
            // Only catch event.
        }
    }
}