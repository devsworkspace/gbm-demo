package com.alex.gbmdemo.components

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import com.alex.gbmdemo.R
import java.text.NumberFormat

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@BindingMethods(
    value = [
        BindingMethod(
            type = NumericTextView::class,
            attribute = "value",
            method = "setValue"
        )
    ]
)
class NumericTextView(context: Context, attributeSet: AttributeSet) :
    AppCompatTextView(context, attributeSet) {


    private var numberFormat: NumberFormat? = null
        get() {
            if (field == null)
                field = NumberFormat.getInstance(resources.configuration.locales[0]).apply {
                    maximumFractionDigits = 2
                    minimumFractionDigits = 2
                }
            return field
        }


    var value: Number? = null
        set(value) {
            field = value

            text = if (field != null && field != 0.0)
                getTextInfo(field!!)
            else
                ""
        }

    private fun getTextInfo(value: Number): String? {
        val numberFormat = numberFormat?.format(value)
        if (resourceLabel == null)
            return numberFormat

        return resources.getString(resourceLabel!!, numberFormat)
    }

    private var resourceLabel: Int? = null

    init {
        val typedArray = context.theme.obtainStyledAttributes(
            attributeSet,
            R.styleable.NumericTextView,
            0,
            0
        )

        val resourceId = typedArray.getResourceId(R.styleable.NumericTextView_text_label, -1)

        if (resourceId > 0)
            resourceLabel = resourceId
    }

}