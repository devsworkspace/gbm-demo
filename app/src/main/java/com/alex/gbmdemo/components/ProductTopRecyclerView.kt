package com.alex.gbmdemo.components

import android.content.Context
import android.util.AttributeSet
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import androidx.recyclerview.widget.RecyclerView
import com.alex.domain.models.FinancialProduct
import com.alex.gbmdemo.adapters.ProductTopAdapter

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 24/11/21.
 */
@BindingMethods(
    value = [
        BindingMethod(
            type = ProductTopRecyclerView::class,
            attribute = "values",
            method = "setProducts"
        )
    ]
)
class ProductTopRecyclerView(context: Context, attrs: AttributeSet) : RecyclerView(context, attrs) {

    private val productAdapter = ProductTopAdapter()

    var products: List<FinancialProduct>?
        get() = productAdapter.products
        set(value) {
            productAdapter.products = value?.toMutableList()
        }

    init {
        setHasFixedSize(true)
        adapter = productAdapter
    }

}