package com.alex.gbmdemo.extensions

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 22/11/21.
 */
fun Date.localeDateFormat(formatStyle: FormatStyle): String =
    localFormat(
        DateTimeFormatter
            .ofLocalizedDate(formatStyle)
            .withZone(ZoneId.systemDefault())
    )


fun Date.localeTimeFormat(formatStyle: FormatStyle): String =
    localFormat(
        DateTimeFormatter
            .ofLocalizedTime(formatStyle)
            .withZone(ZoneId.systemDefault())
    )

fun Date.localFormat(dateTimeFormatter: DateTimeFormatter): String =
    toLocalDateTime().format(dateTimeFormatter)

fun Date.toLocalDateTime(): LocalDateTime = Instant
    .ofEpochMilli(this.time)
    .atZone(ZoneId.systemDefault())
    .toLocalDateTime()