package com.alex.gbmdemo.extensions

import android.app.AlertDialog
import androidx.fragment.app.Fragment
import com.alex.gbmdemo.R
import com.alex.gbmdemo.utils.validation

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 20/11/21.
 */
fun Fragment.showDialog(
    message: String,
    title: String = getString(R.string.app_name),
    onAccept: (() -> Unit)? = null,
    showCancelButton: Boolean = false,
    positiveLabelButton: String = getString(R.string.btn_accept)
) {
    AlertDialog
        .Builder(requireContext())
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(positiveLabelButton) { dialog, _ ->
            onAccept?.invoke()
            dialog.dismiss()
        }.validation(showCancelButton) {
            setNegativeButton(getString(R.string.btn_cancel)) { dialog, _ ->
                dialog.dismiss()
            }
        }.setCancelable(false)
        .create()
        .show()
}