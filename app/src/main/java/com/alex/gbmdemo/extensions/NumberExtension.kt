package com.alex.gbmdemo.extensions

import com.nageshempire.kmnumbers.KMNumbers
import java.text.NumberFormat

private var numberFormat: NumberFormat? = null
    get() {
        if (field == null)
            field = NumberFormat.getCurrencyInstance()
        return field
    }

fun Number.currencyFormat(): String = numberFormat?.format(this) ?: ""

fun Number.toShortSocialNetwork(): String =
    if (this.toLong() >= 1000) KMNumbers.formatNumbers(this.toLong()) else this.toString()


