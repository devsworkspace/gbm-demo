package com.alex.gbmdemo.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import com.alex.gbmdemo.R
import com.alex.gbmdemo.databinding.FragmentChartBinding
import com.alex.gbmdemo.viewmodels.ChartViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ChartFragment : HandleRepositoryErrorFragment() {

    private val viewModel: ChartViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    confirmLogout()
                }
            })

        viewModel.chartUIState.observe(this) {
            when (it) {
                is ChartViewModel.ChartUIState.Error -> handleRepositoryError(it.throwable)
                ChartViewModel.ChartUIState.Loading -> loader?.visibility = View.VISIBLE
                ChartViewModel.ChartUIState.Success -> loader?.visibility = View.GONE
            }
        }
        requireActivity().title = getString(R.string.label_piece_of_cake)
    }

    override fun retryAction() {
        viewModel.loadInfoChart()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentChartBinding.inflate(
        inflater,
        container,
        false
    ).apply {
        vm = viewModel
    }.root
}