package com.alex.gbmdemo.fragments

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.alex.gbmdemo.R
import com.alex.gbmdemo.components.LoaderView
import com.alex.gbmdemo.extensions.showDialog
import retrofit2.HttpException
import java.io.IOException

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
abstract class HandleRepositoryErrorFragment : Fragment() {

    protected var loader: LoaderView? = null
        get() {
            if (field == null)
                field =
                    requireActivity().findViewById(R.id.lout_loader)
                        ?: LoaderView(requireContext()).apply {
                            id = R.id.lout_loader
                            visibility = View.GONE
                            requireActivity().addContentView(
                                this, ViewGroup.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT
                                )
                            )
                        }
            return field
        }

    protected fun handleRepositoryError(throwable: Throwable) {
        loader?.visibility = View.GONE
        when (throwable) {
            is IOException -> showDialog(
                getString(R.string.msg_connection_error),
                onAccept = { retryAction() },
                positiveLabelButton = getString(R.string.btn_retry)
            )
            is HttpException -> showDialog(getString(R.string.msg_server_error, throwable.code()))
            else -> {
                showDialog(getString(R.string.msg_unexpected_error))
            }
        }
    }

    protected fun confirmLogout() {
        showDialog(
            message = getString(R.string.msg_logout),
            onAccept = { requireActivity().finish() },
            showCancelButton = true
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        confirmLogout()
        return super.onOptionsItemSelected(item)
    }

    protected abstract fun retryAction()


}