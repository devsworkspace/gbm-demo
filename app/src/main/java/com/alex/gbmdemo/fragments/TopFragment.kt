package com.alex.gbmdemo.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.widget.ViewPager2
import com.alex.gbmdemo.R
import com.alex.gbmdemo.adapters.TopFragmentAdapter
import com.alex.gbmdemo.viewmodels.TopViewModel
import com.google.android.material.tabs.TabLayoutMediator


class TopFragment : HandleRepositoryErrorFragment() {

    private val viewModel: TopViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.topUIState.observe(this) {
            when (it) {
                is TopViewModel.TopUIState.Error -> handleRepositoryError(it.throwable)
                TopViewModel.TopUIState.Loading -> loader?.visibility = View.VISIBLE
                TopViewModel.TopUIState.Success -> loader?.visibility = View.GONE
            }
        }

        requireActivity().title = getString(R.string.label_damm_im_good)
    }

    override fun retryAction() {
        viewModel.loadFinancialProductsTop()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_top, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        TabLayoutMediator(
            view.findViewById(R.id.tb_top_list),
            view.findViewById<ViewPager2>(R.id.vp_list_tops).apply {
                adapter = TopFragmentAdapter(this@TopFragment)
            }) { tab, position ->
            when (position) {
                0 -> tab.text = resources.getString(R.string.label_increases)
                1 -> tab.text = resources.getString(R.string.label_lows)
                else -> tab.text = resources.getString(R.string.label_volume)
            }
        }.attach()

    }

}