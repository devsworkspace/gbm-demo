package com.alex.gbmdemo.providers

import com.alex.data.apis.CodeChallengeApi
import com.alex.data.endpoints.CodeChallengeEndpoint
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@Module
@InstallIn(ViewModelComponent::class)
class ApiProvider {

    @Provides
    fun provideCodeChallengeApiProvider(
        codeChallengeEndpoint: CodeChallengeEndpoint
    ): CodeChallengeApi {
        return CodeChallengeApi(codeChallengeEndpoint)
    }

}