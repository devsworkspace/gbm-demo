package com.alex.gbmdemo.providers

import com.alex.data.endpoints.CodeChallengeEndpoint
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import retrofit2.Retrofit

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 20/11/21.
 */
@Module
@InstallIn(ViewModelComponent::class)
class EndpointProvider {

    @Provides
    fun provideCodeChallengeEndpoint(
        retrofit: Retrofit
    ): CodeChallengeEndpoint {
        return retrofit.create(CodeChallengeEndpoint::class.java)
    }

}