package com.alex.gbmdemo.providers

import com.alex.data.apis.CodeChallengeApi
import com.alex.data.daos.ConsumerPriceIndexDao
import com.alex.data.repositories.ConsumerPriceRepositoryImp
import com.alex.data.repositories.FinancialProductRepositoryImp
import com.alex.domain.repositories.ConsumerPriceRepository
import com.alex.domain.repositories.FinancialProductRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@Module
@InstallIn(ViewModelComponent::class)
class RepositoryProvider {

    @Provides
    fun provideConsumerPriceRepository(
        codeChallengeApi: CodeChallengeApi,
        consumerPriceIndexDao: ConsumerPriceIndexDao
    ): ConsumerPriceRepository {
        return ConsumerPriceRepositoryImp(codeChallengeApi, consumerPriceIndexDao)
    }

    @Provides
    fun provideFinancialProductRepository(
        codeChallengeApi: CodeChallengeApi
    ): FinancialProductRepository {
        return FinancialProductRepositoryImp(codeChallengeApi)
    }

}