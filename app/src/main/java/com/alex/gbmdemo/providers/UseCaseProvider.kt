package com.alex.gbmdemo.providers

import com.alex.domain.repositories.ConsumerPriceRepository
import com.alex.domain.repositories.FinancialProductRepository
import com.alex.domain.usecases.GetConsumerPriceHistoryUseCase
import com.alex.domain.usecases.GetFinancialProductsTop
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@Module
@InstallIn(ViewModelComponent::class)
class UseCaseProvider {

    @Provides
    fun provideGetConsumerPriceHistoryUseCase(
        consumerPriceRepository: ConsumerPriceRepository
    ): GetConsumerPriceHistoryUseCase {
        return GetConsumerPriceHistoryUseCase(consumerPriceRepository)
    }

    @Provides
    fun provideGetFinancialProductsTop(
        financialProductRepository: FinancialProductRepository
    ): GetFinancialProductsTop {
        return GetFinancialProductsTop(financialProductRepository)
    }

}