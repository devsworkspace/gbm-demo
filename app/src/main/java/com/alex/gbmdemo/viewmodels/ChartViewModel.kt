package com.alex.gbmdemo.viewmodels

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alex.domain.models.ConsumerPriceIndex
import com.alex.domain.models.ConsumerPriceIndexHistory
import com.alex.domain.usecases.GetConsumerPriceHistoryUseCase
import com.alex.gbmdemo.utils.LiveEvent
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@HiltViewModel
class ChartViewModel
@Inject
constructor(
    private val getConsumerPriceHistoryUseCase: GetConsumerPriceHistoryUseCase
) : ViewModel() {

    private val _chartUIState: LiveEvent<ChartUIState> by lazy {
        LiveEvent<ChartUIState>().apply {
            loadInfoChart(this)
        }
    }

    val chartUIState: LiveData<ChartUIState>
        get() = _chartUIState

    val selectedConsumerPrice = ObservableField<ConsumerPriceIndex>()
    val consumerPriceIndexHistoryField = ObservableField<ConsumerPriceIndexHistory>()
    val onConsumerPriceIndexSelected: (ConsumerPriceIndex) -> Unit = {
        selectedConsumerPrice.set(it)
    }

    fun loadInfoChart(chartUIStateParam: LiveEvent<ChartUIState>? = null) =
        viewModelScope.launch(Dispatchers.IO) {
            val chartUIState = chartUIStateParam ?: _chartUIState
            chartUIState.postValue(ChartUIState.Loading)
            try {
                val consumerPriceHistory = getConsumerPriceHistoryUseCase()
                consumerPriceIndexHistoryField.set(consumerPriceHistory)
                selectedConsumerPrice.set(consumerPriceHistory.consumerPriceIndex.last())
                chartUIState.postValue(ChartUIState.Success)
            } catch (throwable: Throwable) {
                Timber.e(throwable)
                FirebaseCrashlytics.getInstance().recordException(throwable);
                chartUIState.postValue(ChartUIState.Error(throwable))
            }
        }

    sealed class ChartUIState {
        object Success : ChartUIState()
        data class Error(val throwable: Throwable) : ChartUIState()
        object Loading : ChartUIState()
    }

}