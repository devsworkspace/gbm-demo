package com.alex.gbmdemo.viewmodels

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alex.domain.models.FinancialProduct
import com.alex.domain.models.FinancialProduct.TypeRiseLow
import com.alex.domain.usecases.GetFinancialProductsTop
import com.alex.gbmdemo.utils.LiveEvent
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 24/11/21.
 */
@HiltViewModel
class TopViewModel
@Inject
constructor(
    private val getFinancialProductsTop: GetFinancialProductsTop
) : ViewModel() {

    private val _topUIState: LiveEvent<TopUIState> by lazy {
        LiveEvent<TopUIState>().apply {
            loadFinancialProductsTop(this)
        }
    }

    private val increasesField = ObservableField<List<FinancialProduct>>()
    private val lowsField = ObservableField<List<FinancialProduct>>()
    private val volumeField = ObservableField<List<FinancialProduct>>()

    fun getObservableByType(type: TypeRiseLow): ObservableField<List<FinancialProduct>> {
        return when (type) {
            TypeRiseLow.INCREASES -> increasesField
            TypeRiseLow.LOWS -> lowsField
            TypeRiseLow.VOLUME -> volumeField
        }
    }

    val topUIState: LiveData<TopUIState>
        get() = _topUIState

    fun loadFinancialProductsTop(uiStateParam: LiveEvent<TopUIState>? = null) =
        viewModelScope.launch(Dispatchers.IO) {
            val uiState = uiStateParam ?: _topUIState
            uiState.postValue(TopUIState.Loading)
            try {
                getFinancialProductsTop().collect {
                    increasesField.set(it[TypeRiseLow.INCREASES])
                    lowsField.set(it[TypeRiseLow.LOWS])
                    volumeField.set(it[TypeRiseLow.VOLUME])
                    uiState.postValue(TopUIState.Success)
                }
            } catch (throwable: Throwable) {
                Timber.e(throwable)
                FirebaseCrashlytics.getInstance().recordException(throwable);
                uiState.postValue(TopUIState.Error(throwable))
            }
        }

    sealed class TopUIState {
        object Success : TopUIState()
        object Loading : TopUIState()
        data class Error(val throwable: Throwable) : TopUIState()
    }

}