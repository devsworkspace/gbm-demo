package com.alex.data.apis

import com.alex.data.endpoints.CodeChallengeEndpoint
import com.alex.data.response.ConsumerPriceIndexResponse
import com.alex.data.response.FinancialProductResponse

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 20/11/21.
 */
class CodeChallengeApi(
    private val codeChallengeEndpoint: CodeChallengeEndpoint
) {

    suspend fun getConsumerPriceHistory(): List<ConsumerPriceIndexResponse> {
        return codeChallengeEndpoint.getConsumerPriceHistory()
    }

    suspend fun getFinancialProducts(): List<FinancialProductResponse> {
        return codeChallengeEndpoint.getFinancialProducts()
    }

}