package com.alex.data.daos

import androidx.room.*
import com.alex.data.entities.ConsumerPriceIndexEntity

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@Dao
interface ConsumerPriceIndexDao {

    @Insert
    suspend fun insert(consumerPriceIndexHistory: List<ConsumerPriceIndexEntity>)

    @Delete
    suspend fun delete(consumerPriceIndexHistory: List<ConsumerPriceIndexEntity>)

    @Transaction
    suspend fun updateLocalInfo(consumerPriceIndexHistory: List<ConsumerPriceIndexEntity>) {
        val localInfo = loadLocalInfo()
        if (!localInfo.isNullOrEmpty())
            delete(localInfo)
        insert(consumerPriceIndexHistory)
    }

    @Query("SELECT * FROM ${ConsumerPriceIndexEntity.TABLE_NAME}")
    fun loadLocalInfo(): List<ConsumerPriceIndexEntity>?

}