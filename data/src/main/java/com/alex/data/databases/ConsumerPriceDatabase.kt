package com.alex.data.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import com.alex.data.daos.ConsumerPriceIndexDao
import com.alex.data.entities.ConsumerPriceIndexEntity

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
@Database(
    entities = [ConsumerPriceIndexEntity::class],
    version = 1
)
abstract class ConsumerPriceDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "CONSUMER_PRICE_DB"
    }

    abstract fun getConsumerPriceIndexDao(): ConsumerPriceIndexDao
}