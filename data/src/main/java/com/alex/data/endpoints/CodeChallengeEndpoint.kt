package com.alex.data.endpoints

import com.alex.data.response.ConsumerPriceIndexResponse
import com.alex.data.response.FinancialProductResponse
import retrofit2.http.GET

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 20/11/21.
 */
interface CodeChallengeEndpoint {

    @GET("cc4c350b-1f11-42a0-a1aa-f8593eafeb1e")
    suspend fun getConsumerPriceHistory(): List<ConsumerPriceIndexResponse>

    @GET("b4eb963c-4aee-4b60-a378-20cb5b00678f")
    suspend fun getFinancialProducts(): List<FinancialProductResponse>

}