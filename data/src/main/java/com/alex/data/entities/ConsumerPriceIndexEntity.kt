package com.alex.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/09/21.
 */
@Entity(tableName = ConsumerPriceIndexEntity.TABLE_NAME)
data class ConsumerPriceIndexEntity(
    val change: Double,
    val date: Long,
    @ColumnInfo(name = "percentage_change")
    val percentageChange: Double,
    val price: Double,
    val volume: Int,
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null
) {
    companion object {
        const val TABLE_NAME = "CONSUMER_PRICE_INDEX"
    }
}