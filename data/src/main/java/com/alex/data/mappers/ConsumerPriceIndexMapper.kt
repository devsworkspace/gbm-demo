package com.alex.data.mappers

import com.alex.data.entities.ConsumerPriceIndexEntity
import com.alex.data.response.ConsumerPriceIndexResponse
import com.alex.domain.models.ConsumerPriceIndex
import java.util.*

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 21/11/21.
 */
fun ConsumerPriceIndex.toConsumerPriceIndexEntity(): ConsumerPriceIndexEntity {
    return ConsumerPriceIndexEntity(
        this.change,
        this.date.time,
        this.percentageChange,
        this.price,
        this.volume
    )
}

fun ConsumerPriceIndexEntity.toConsumerPriceIndex(): ConsumerPriceIndex {
    return ConsumerPriceIndex(
        this.change,
        Date(this.date),
        this.percentageChange,
        this.price,
        this.volume
    )
}

fun ConsumerPriceIndexResponse.toConsumerPriceIndex(): ConsumerPriceIndex {
    return ConsumerPriceIndex(
        this.change,
        this.date,
        this.percentageChange,
        this.price,
        this.volume
    )
}

