package com.alex.data.mappers

import com.alex.data.response.FinancialProductResponse
import com.alex.domain.models.FinancialProduct

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 24/11/21.
 */
fun FinancialProductResponse.toFinancialProduct() = FinancialProduct(
    this.issueId,
    this.openPrice,
    this.maxPrice,
    this.minPrice,
    this.percentageChange,
    this.valueChange,
    this.aggregatedVolume,
    this.bidPrice,
    this.bidVolume,
    this.askPrice,
    this.askVolume,
    this.ipcParticipationRate, this.lastPrice, this.closePrice,
    when (this.riseLowTypeId) {
        1 -> FinancialProduct.TypeRiseLow.LOWS
        2 -> FinancialProduct.TypeRiseLow.INCREASES
        else -> FinancialProduct.TypeRiseLow.VOLUME
    },
    this.instrumentTypeId,
    this.benchmarkId,
    this.benchmarkPercentage
)