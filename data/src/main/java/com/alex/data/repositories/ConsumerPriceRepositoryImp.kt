package com.alex.data.repositories

import com.alex.data.apis.CodeChallengeApi
import com.alex.data.daos.ConsumerPriceIndexDao
import com.alex.data.mappers.toConsumerPriceIndex
import com.alex.data.mappers.toConsumerPriceIndexEntity
import com.alex.domain.models.ConsumerPriceIndexHistory
import com.alex.domain.repositories.ConsumerPriceRepository

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 20/11/21.
 */
class ConsumerPriceRepositoryImp(
    private val codeChallengeApi: CodeChallengeApi,
    private val consumerPriceIndexDao: ConsumerPriceIndexDao
) : ConsumerPriceRepository {

    override suspend fun getConsumerPriceHistory(): ConsumerPriceIndexHistory {
        return try {
            val consumerPriceHistory = codeChallengeApi.getConsumerPriceHistory()
                .map { it.toConsumerPriceIndex() }
            consumerPriceIndexDao.updateLocalInfo(consumerPriceHistory.map { it.toConsumerPriceIndexEntity() })
            ConsumerPriceIndexHistory(consumerPriceHistory, false)
        } catch (throwable: Throwable) {
            val loadInfo = consumerPriceIndexDao.loadLocalInfo()?.map { it.toConsumerPriceIndex() }
            if (loadInfo.isNullOrEmpty())
                throw throwable

            ConsumerPriceIndexHistory(loadInfo, true)
        }
    }

}