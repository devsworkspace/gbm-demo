package com.alex.data.repositories

import com.alex.data.apis.CodeChallengeApi
import com.alex.data.mappers.toFinancialProduct
import com.alex.domain.models.FinancialProduct
import com.alex.domain.repositories.FinancialProductRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 23/11/21.
 */
class FinancialProductRepositoryImp(
    private val codeChallengeApi: CodeChallengeApi
) : FinancialProductRepository {

    override suspend fun getFlowTopFinancialProduct(delay: Long): Flow<List<FinancialProduct>> =
        flow {
            while (true) {
                emit(codeChallengeApi.getFinancialProducts().map { it.toFinancialProduct() })
                delay(delay)
            }
        }

}