package com.alex.data.response

import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class ConsumerPriceIndexResponse(
    val change: Double,
    val date: Date,
    val percentageChange: Double,
    val price: Double,
    val volume: Int
)