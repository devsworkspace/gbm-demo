package com.alex.data.response

data class FinancialProductResponse(
    val issueId: String,
    val openPrice: Double,
    val maxPrice: Double,
    val minPrice: Double,
    val percentageChange: Double,
    val valueChange: Double,
    val aggregatedVolume: Int,
    val bidPrice: Double,
    val bidVolume: Int,
    val askPrice: Double,
    val askVolume: Int,
    val ipcParticipationRate: Double,
    val lastPrice: Double,
    val closePrice: Double,
    val riseLowTypeId: Int,
    val instrumentTypeId: Int,
    val benchmarkId: Int,
    val benchmarkPercentage: Int
) {
}