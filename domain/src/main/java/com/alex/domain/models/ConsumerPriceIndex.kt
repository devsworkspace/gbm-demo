package com.alex.domain.models

import java.util.*

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 20/11/21.
 */
data class ConsumerPriceIndex(
    val change: Double,
    val date: Date,
    val percentageChange: Double,
    val price: Double,
    val volume: Int
)