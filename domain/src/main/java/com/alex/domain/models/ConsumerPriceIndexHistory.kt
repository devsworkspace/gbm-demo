package com.alex.domain.models

/**
 *  Model that encompasses consumer price index
 *  information and the information origen.
 *
 * @param consumerPriceIndex All consumer price indices
 * @param localInfo true: The information is obtained
 * from the device cache | false The information is obtained
 * from Internet
 *
 * @author Alejandro Salazar (salazaralejandro767@gmail.com) on 20/11/21.
 */
data class ConsumerPriceIndexHistory(
    val consumerPriceIndex: List<ConsumerPriceIndex>,
    val localInfo: Boolean
)