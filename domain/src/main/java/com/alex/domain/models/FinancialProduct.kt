package com.alex.domain.models

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com)
 */
data class FinancialProduct(
    val issueId: String,
    val openPrice: Double,
    val maxPrice: Double,
    val minPrice: Double,
    val percentageChange: Double,
    val valueChange: Double,
    val aggregatedVolume: Int,
    val bidPrice: Double,
    val bidVolume: Int,
    val askPrice: Double,
    val askVolume: Int,
    val ipcParticipationRate: Double,
    val lastPrice: Double,
    val closePrice: Double,
    val riseLowType: TypeRiseLow,
    val instrumentTypeId: Int,
    val benchmarkId: Int,
    val benchmarkPercentage: Int
) {

    override fun equals(other: Any?): Boolean {
        return other is FinancialProduct
                && other.issueId == other.issueId
                && other.riseLowType == this.riseLowType
                && other.aggregatedVolume == this.aggregatedVolume
                && other.percentageChange == this.percentageChange
                && other.lastPrice == this.lastPrice
    }

    enum class TypeRiseLow {
        INCREASES,
        LOWS,
        VOLUME
    }
}