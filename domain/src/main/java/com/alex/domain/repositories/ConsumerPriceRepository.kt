package com.alex.domain.repositories

import com.alex.domain.models.ConsumerPriceIndexHistory

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 20/11/21.
 */
interface ConsumerPriceRepository {

    suspend fun getConsumerPriceHistory(): ConsumerPriceIndexHistory


}