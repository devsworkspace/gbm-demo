package com.alex.domain.repositories

import com.alex.domain.models.FinancialProduct
import kotlinx.coroutines.flow.Flow

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 23/11/21.
 */
interface FinancialProductRepository {

    suspend fun getFlowTopFinancialProduct(delay:Long): Flow<List<FinancialProduct>>
}