package com.alex.domain.usecases

import com.alex.domain.models.ConsumerPriceIndexHistory
import com.alex.domain.repositories.ConsumerPriceRepository

/**
 * This class return the consumer price history.
 * @return The consumer price history ([ConsumerPriceIndexHistory]).
 * @author Alejandro Salazar (salazaralejandro767@gmail.com)
 */
class GetConsumerPriceHistoryUseCase(
    private val consumerPriceRepository: ConsumerPriceRepository
) {

    suspend operator fun invoke(): ConsumerPriceIndexHistory {
        return consumerPriceRepository.getConsumerPriceHistory()
    }

}