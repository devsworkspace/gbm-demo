package com.alex.domain.usecases

import com.alex.domain.models.FinancialProduct
import com.alex.domain.models.FinancialProduct.TypeRiseLow
import com.alex.domain.repositories.FinancialProductRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

/**
 * This class determine the top ten financial
 * products from three different categories.
 *
 * @param delay Time in milliseconds between updates. Default 15s.
 * @return A flow to update the information. The flow
 * provides a map with the three categories
 *
 * @author Alejandro Salazar (salazaralejandro767@gmail.com)
 */
class GetFinancialProductsTop(
    private val financialProductRepository: FinancialProductRepository
) {

    suspend operator fun invoke(delay: Long = 15000): Flow<Map<TypeRiseLow, List<FinancialProduct>>> {
        return financialProductRepository.getFlowTopFinancialProduct(delay)
            .map { financialProducts ->
                hashMapOf(
                    TypeRiseLow.INCREASES to financialProducts.filter { it.riseLowType == TypeRiseLow.INCREASES }
                        .sortedByDescending { it.percentageChange }.take(10),
                    TypeRiseLow.LOWS to financialProducts.filter { it.riseLowType == TypeRiseLow.LOWS }
                        .sortedBy { it.percentageChange }.take(10),
                    TypeRiseLow.VOLUME to financialProducts.filter { it.riseLowType == TypeRiseLow.VOLUME }
                        .sortedByDescending { it.aggregatedVolume }.take(10)
                )
            }
    }
}