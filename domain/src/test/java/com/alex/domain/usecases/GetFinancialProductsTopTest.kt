package com.alex.domain.usecases

import com.alex.domain.DataDummy
import com.alex.domain.models.FinancialProduct.TypeRiseLow
import com.alex.domain.repositories.FinancialProductRepository
import com.google.common.truth.Truth.assertThat
import junit.framework.TestCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Alejandro Salazar (salazaralejandro767@gmail.com) on 23/11/21.
 */
@RunWith(MockitoJUnitRunner::class)
class GetFinancialProductsTopTest : TestCase() {

    private lateinit var getFinancialProductsTop: GetFinancialProductsTop

    @Mock
    private lateinit var financialProductRepositoryDummy: FinancialProductRepository

    @Before
    fun setup() = runBlocking {
        Mockito.`when`(financialProductRepositoryDummy.getFlowTopFinancialProduct(5000))
            .thenReturn(flow { emit(DataDummy.getDataDummy()) })
        getFinancialProductsTop = GetFinancialProductsTop(financialProductRepositoryDummy)
    }

    @Test
    fun `get only ten financial products INCREASE`() = runBlocking {
        getFinancialProductsTop().collect {
            assertThat(it[TypeRiseLow.INCREASES]?.size).isEqualTo(10)
        }
    }

    @Test
    fun `get only ten financial products LOWS`() = runBlocking {
        getFinancialProductsTop().collect {
            assertThat(it[TypeRiseLow.LOWS]?.size).isEqualTo(10)
        }
    }

    @Test
    fun `get only ten financial products VOLUME`() = runBlocking {
        getFinancialProductsTop().collect {
            assertThat(it[TypeRiseLow.VOLUME]?.size).isEqualTo(10)
        }
    }


    @Test
    fun `order financial products INCREASES by percentage change descending`() = runBlocking {
        getFinancialProductsTop().collect {
            val increases = it[TypeRiseLow.INCREASES]

            for (i in 0..increases!!.size - 2) {
                assertThat(increases[i].percentageChange).isGreaterThan(increases[i + 1].percentageChange)
            }
        }
    }

    @Test
    fun `order financial products LOWS by percentage change ascending`() = runBlocking {
        getFinancialProductsTop().collect {
            val lows = it[TypeRiseLow.LOWS]

            for (i in 0..lows!!.size - 2) {
                assertThat(lows[i].percentageChange).isLessThan(lows[i + 1].percentageChange)
            }
        }
    }

    @Test
    fun `order financial products VOLUME by aggregated volume descending`() = runBlocking {
        getFinancialProductsTop().collect {
            val volume = it[TypeRiseLow.VOLUME]

            for (i in 0..volume!!.size - 2) {
                assertThat(volume[i].aggregatedVolume).isGreaterThan(volume[i + 1].aggregatedVolume)
            }
        }
    }
}